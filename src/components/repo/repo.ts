import { Component } from '@angular/core';

/**
 * Generated class for the RepoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'repo',
  templateUrl: 'repo.html'
})
export class RepoComponent {

  text: string;

  constructor() {
    console.log('Hello RepoComponent Component');
    this.text = 'Hello World';
  }

}
