import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { GithubServiceProvider } from '../../providers/github-service/github-service';
import { User } from '../../models/user.interface';
import { Repository } from '../../models/repository.interface';
/**
 * Generated class for the ProfileSearchResultsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-search-results',
  templateUrl: 'profile-search-results.html',
})
export class ProfileSearchResultsPage {
personname:string;
user:User;
repository:Repository[];
  constructor(private Github:GithubServiceProvider, private navCtrl: NavController, private navParams: NavParams) {
  }

  // GitUserInformation():void{
  //   this.Github.mockGitUserIformation(this.personname).subscribe(data=>console.log(data));
  // }
  GitUserInformation():void{
    this.Github.gitUserInformation(this.personname).subscribe((data:User)=> this.user=data);
    // this.Github.mockGitUserIformation(this.personname).subscribe((data:User)=>this.user=data);
    // this.Github.mockGitRepositoryInformation(this.personname).subscribe((data:Repository[])=>this.repository= data);
  }

  ionViewWillLoad() {
    
  this.personname=this.navParams.get('personname');
  //console.log(this.personname);
  if(this.personname){
    this.GitUserInformation();
  }
  }

}
