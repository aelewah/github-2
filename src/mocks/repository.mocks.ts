import { Repository } from "../models/repository.interface";
import {USER_LIST} from "./user.mocks";

const repositorylist:Repository[]=[
    {
        name:'ionic three',
        description:'ay betngan',
        owner:USER_LIST[0]
    },
    {
        name:'GSM',
        description:'ay betngan',
        owner:USER_LIST[1]
    },
    {
        name:'Wifi',
        description:'ay betngan',
        owner:USER_LIST[0]
    },
    {
        name:'Bluetooth',
        description:'ay betngan',
        owner:USER_LIST[1]
    }

]
export const REPOLIST=repositorylist;