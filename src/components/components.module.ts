import { NgModule } from "@angular/core";
import {SearchResultsComponent} from './search-results/search-results-component';
import {IonicModule} from 'ionic-angular';
import { RepositoriesComponent } from './repositories/repositories-component';
import { RepoComponent } from './repo/repo';


@NgModule({
declarations:[
    SearchResultsComponent,
    RepositoriesComponent,
    RepoComponent
    
],
imports:[
    IonicModule
],
exports:[
    SearchResultsComponent,
    RepositoriesComponent,
    RepoComponent
    
]
})

export class Componentsmodule {

}