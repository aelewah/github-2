export interface User{
    url:string;
    name: string;
    email: string;
    company:string;
    bio:string;
    avatar_url:string;

}