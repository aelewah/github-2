import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileSearchResultsPage } from './profile-search-results';
import { Componentsmodule} from '../../components/components.module';
// import {SearchResultsComponent} from '../../components/search-results/search-results.component';
@NgModule({
  declarations: [
    ProfileSearchResultsPage,
    
  ],
  imports: [
    IonicPageModule.forChild(ProfileSearchResultsPage),
    Componentsmodule
    //SearchResultsComponent
  ],
})
export class ProfileSearchResultsPageModule {}
