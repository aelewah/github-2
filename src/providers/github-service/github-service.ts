import { Http,Response } from '@angular/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import {User} from '../../models/user.interface';
import {USER_LIST} from '../../mocks/user.mocks';
import { Repository } from '../../models/repository.interface';
import { REPOLIST } from '../../mocks/repository.mocks';
//import {}  from '../../models/repository.interface'
/*
  Generated class for the GithubServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GithubServiceProvider {
  private baseUrl:string ="https://api.github.com/users";

  constructor(private http:Http) {
    console.log('Hello GithubServiceProvider Provider');
  }
  gitUserInformation(username :string):Observable<User>{
    return this.http.get(`${this.baseUrl}/${username}`)
    .do((data:Response)=> console.log(data))
    .map((data:Response)=> data.json())
    .do((data:Response)=> console.log(data))
    .catch((error:Response)=> Observable.throw(error.json().error || "Server error."))

  }
  mockGitUserIformation(username :string):Observable<User> {
return Observable.of(USER_LIST.filter(user => user.name === username)[0]) ;
  }
  mockGitRepositoryInformation(username: string):Observable<Repository[]> {
    return Observable.of(REPOLIST.filter(repository => repository.owner.name === username));

  }

  // mockGitUserIformation(username :string):Observable<Reposito> {
  //   return Observable.of(USER_LIST.filter(user => user.name === username)[0]) ;
  //     }

}
